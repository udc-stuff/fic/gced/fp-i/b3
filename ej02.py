

def foo(day: str) -> str:
	if day == "L" or day == "l":
		return "Lunes"
	elif day == "M" or day == "m":
		return "Martes"
	elif day == "X" or day == "x":
		return "Miércoles"
	elif day == "J" or day == "j":
		return "Jueves"
	elif day == "V" or day == "v":
		return "Viernes"
	elif day == "S" or day == "s":
		return "Sábado"
	elif day == "D" or day == "d":
		return "Domingo"
	else:
		return "[" + day + ": dato no valido]"


if __name__ == "__main__":
	s = input("Introduce uno de los siguientes identificadores (L,M,X,J,V,S,D) : ")
	print(s, "es", foo(s))

	
