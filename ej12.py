

def is_vowel(c: str) -> bool:
	return c in ['A', 'a', 'E', 'e', 'I', 'i', 'O', 'o', 'U', 'u']

if __name__ == "__main__":
	c = input("Introduce un caracter: ")

	if is_vowel(c):
		print(c, "es una vocal")
	else:
		print(c, "no es una vocal")

