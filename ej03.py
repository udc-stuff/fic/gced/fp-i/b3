

def divisor(number: int) -> int:
	if number <= 1:
		return -1

	cnt = number - 1
	while cnt > 1:
		if number % cnt == 0:
			return cnt
		cnt -= 1

	return cnt

if __name__ == "__main__":
	n = int(input("Introduce un numero entero positivo: "))
	aux = divisor(n)

	if aux == -1:
		print(n, "numero no valido")
	else:
		print("El mayor divisor de", n, "es", aux)

