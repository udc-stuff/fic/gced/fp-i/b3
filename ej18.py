
def fib(n: int) -> int:
	if n == 0 or n == 1:
		return n

	n1 = 0
	n2 = 1
	for cnt in range(n-1):
		n1, n2 = n2, n1+n2

	return n2


if __name__ == "__main__":
	n = int(input("Introduce un numero: "))

	print("El termino", n, "de la sucesion de fibonacci es", fib(n))

 
