

def foo(n: int) -> int:
	if n < 10:
		return n
	else:
		return foo(n // 10) + n % 10

if __name__ == "__main__":
	number = int(input("Introduce un numero entero: "))
	print("La suma de las cifras del numero", number, "es", foo(number))

