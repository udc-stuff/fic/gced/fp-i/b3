En este repositorio os encontraréis ejercicios del boletín 3 que se han realizado en clase de prácticas.

Quizás encontréis alguna pequeña modificación con respecto al enunciado.
Recordar que esas pequeñas modificaciones han sido realizadas para explicar cosas concretas que ocurren al usar el lenguaje Python.

# Ejercicio 1.

Plantee e implemente un programa que contenga una función que reciba un parámetro que permita imprimir un determinado número de líneas en blanco.

### Solución

```python


def print_lines(n: int):
	for cnt in range(n):
		print()

if __name__ == "__main__":
	n = int(input("Introduce un numero de lineas a mostrar: "))
	print_lines(n)
```

# Ejercicio 2.

Plantee e implemente un programa que a partir de la inicial del día de la semana (L, M, X, J, V, S, D) introducida mediante el teclado, una función que devuelva por pantalla el nombre del día en cuestión.

### Solución


```python


def foo(day: str) -> str:
	if day == "L" or day == "l":
		return "Lunes"
	elif day == "M" or day == "m":
		return "Martes"
	elif day == "X" or day == "x":
		return "Miércoles"
	elif day == "J" or day == "j":
		return "Jueves"
	elif day == "V" or day == "v":
		return "Viernes"
	elif day == "S" or day == "s":
		return "Sábado"
	elif day == "D" or day == "d":
		return "Domingo"
	else:
		return "[" + day + ": dato no valido]"


if __name__ == "__main__":
	s = input("Introduce uno de los siguientes identificadores (L,M,X,J,V,S,D) : ")
	print(s, "es", foo(s))

	
```

# Ejercicio 3.

Plantee e implemente un programa que solicite un número entero y que devuelva el mayor entero que lo divide (exceptuando el propio número).

### Solución


```python


def divisor(number: int) -> int:
	if number <= 1:
		return -1

	cnt = number - 1
	while cnt > 1:
		if number % cnt == 0:
			return cnt
		cnt -= 1

	return cnt

if __name__ == "__main__":
	n = int(input("Introduce un numero entero positivo: "))
	aux = divisor(n)

	if aux == -1:
		print(n, "numero no valido")
	else:
		print("El mayor divisor de", n, "es", aux)

```

# Ejercicio 4.

Plantee e implemente un programa mostrar el triángulo de Floyd hasta un número entero introducido por teclado.

### Solución


```python


def floyd(n: int):
	cnt = 1
	row = 1
	while cnt <= n:
		col = 1
		while col <= row and cnt <= n:
			print(cnt, end="\t")
			cnt += 1
			col += 1
		print("")
		row += 1


if __name__ == "__main__":
	n = int(input("Introduce un numero para dibujar el triangulo de floyd: "))
	
	floyd(n)
```

# Ejercicio 5.

Plantee e implemente un programa para mostrar un número determinado de filas del triángulo de Floyd.

### Solución


```python


def floyd(line: int):
	cnt = 1
	for row in range(1, line + 1):
		for col in range(row):
			print(cnt, end="\t")
			cnt += 1
		print("")


if __name__ == "__main__":
	n = int(input("Introduce el numero de lineas a mostrar del triangulo de floyd: "))
	
	floyd(n)

```

# Ejercicio 6.

Plantee e implemente un programa que mediante una serie de subprogramas determine si la fecha introducida por teclado es o no correcta, teniendo en cuenta para ello los años bisiestos.

### Solución


```python

def check_date(d: int, m: int, y: int) -> bool:
	if d < 1 or m < 1 or y < 1:
		return False

	if m > 12:
		return False

	if m == 2:
		leap_year = y % 4 == 0 and (y % 100 != 0 or y % 400 == 0)
		if leap_year:
			return d <= 29
		else:
			return d <= 28
	elif m == 4 or m == 6 or m == 9 or m == 11:
		return d <= 30
	else:
		return d <= 31


if __name__ == "__main__":
	d = int(input("Introduce un dia: "))
	m = int(input("Introduce un mes: "))
	y = int(input("Introduce un ano: "))

	if check_date(d, m, y):
		print(d, "/", m, "/", y, " es una fecha valida", sep="")
	else:
		print(d, "/", m, "/", y, " no es una fecha valida", sep="")
```

# Ejercicio 7.

Plantee e implemente una función que reciba un valor que representa un mes y devuelva el siguiente mes, utilizando para ello la siguiente interfaz:

```
next_month(n: int) -> int:
```

NOTA: Téngase en cuenta que el mes siguiente a 12 es 1.

### Solución


```python


def next_month(n: int) -> int:
	return (n % 12) + 1

if __name__ == "__main__":
	n = int(input("Introduce un numero de mes: "))

	print("El mes siguiente a", n, "es", next_month(n))
```

# Ejercicio 8.

Plantee e implemente una función que tenga un argumento de tipo entero y que devuelva la letra P si el número es positivo o N si el número es negativo.

### Solución


```python


def foo(n: int) -> str:
	if n >= 0:
		return 'P'
	else:
		return 'N'


if __name__ == "__main__":
	n = int(input("Introduce un numero entero: "))

	print(n, "es", foo(n))

```

# Ejercicio 9.

Plantee e implemente un programa para leer un entero dentro de un intervalo, cuyos límites inferior y superior los proporcione el usuario.

### Solución


```python


def between_range(minimum: int, maximum: int, s: str = '') -> int:
	if s == '':
		s = "Introduce un numero entero dentro del rango [" + str(minimum) + "," + str(maximum) + "]: "
	
	n = minimum - 1
	while n < minimum or n > maximum:
		n = int(input(s))

	return n

if __name__ == "__main__":
	n = between_range(-10, 10, "Introduce un numero entre -10 y 10: ")
	print("El numero introducido fue: ", n)

	n = between_range(0, 5)
	print("El numero introducido fue: ", n)

```

# Ejercicio 10.

Plantee e implemente un programa que reciba un entero N y calcule 1+2+3+…+N, devolviendo dicho valor.

### Solución


```python



def foo(n: int) -> int:
	total = 0
	for cnt in range(1, n + 1):
		total += cnt

	return total

if __name__ == "__main__":
	n = int(input("Introduce un numero entero: "))

	print("La suma es: ", foo(n))

```

# Ejercicio 11.

Plantee e implemente una función que calcule el valor del siguiente polinomio: 3x<sup>5</sup>-2x<sup>4</sup>+x<sup>3</sup>-2x<sup>2</sup>-3x+4. El usuario ha de proporcionar por teclado el valor de x.

### Solución


```python

def polynomial(x: float) -> float:
	return 3 * pow(x, 5) - 2 * pow(x, 4) + pow(x, 3) - 2 * pow(x, 2) - 3 * x + 4


if __name__ == "__main__":
	n = float(input("Introduce un numero para calcular el polinomio"))

	print('El valor del polinomio es', polynomial(n))

```

# Ejercicio 12.

Plantee e implemente la función *is_vowel* que recibe un carácter e indique si es vocal o no.

### Solución


```python


def is_vowel(c: str) -> bool:
	return c in ['A', 'a', 'E', 'e', 'I', 'i', 'O', 'o', 'U', 'u']

if __name__ == "__main__":
	c = input("Introduce un caracter: ")

	if is_vowel(c):
		print(c, "es una vocal")
	else:
		print(c, "no es una vocal")

```

# Ejercicio 13.

Plantee e implemente la función *is_digit* que recibe un carácter e indique si es un dígito o no.

### Solución


```python


def is_digit(c: str) -> bool:
	tmp = ord(c)
	return tmp >= ord('0') and tmp <= ord('9')

if __name__ == "__main__":
	c = input("Introduce un caracter: ")

	if is_digit(c):
		print(c, "es un digito")
	else:
		print(c, "no es un ditigo")

```

# Ejercicio 14.

Plantee e implemente una función que compruebe si un carácter está en mayúsculas o en minúsculas.

### Solución


```python


def is_upper(c: str) -> bool:
	tmp = ord(c)
	return tmp >= ord('A') and tmp <= ord('Z')

def is_lower(c: str) -> bool:
	tmp = ord(c)
	return tmp >= ord('a') and tmp <= ord('z')

if __name__ == "__main__":
	c = input("Introduce un caracter: ")

	if is_upper(c):
		print(c, "es una letra en mayusculas")
	else:
		print(c, "no es una letra en mayusculas")

	if is_lower(c):
		print(c, "es una letra en minusculas")
	else:
		print(c, "no es una letra en minusculas")


```

# Ejercicio 15.

Plantee e implemente un programa que utilizando las funciones anteriores determinen cuantas letras y dígitos incluye un string.

### Solución


```python


def is_vowel(c: str) -> bool:
	return c in ['A', 'a', 'E', 'e', 'I', 'i', 'O', 'o', 'U', 'u']


def is_digit(c: str) -> bool:
	tmp = ord(c)
	return tmp >= ord('0') and tmp <= ord('9')


def is_upper(c: str) -> bool:
	tmp = ord(c)
	return tmp >= ord('A') and tmp <= ord('Z')


def is_lower(c: str) -> bool:
	tmp = ord(c)
	return tmp >= ord('a') and tmp <= ord('z')


if __name__ == "__main__":
	s = input("Introduce una cadena: ")
	digits = 0
	letters = 0
	vowels = 0

	for item in s:
		if is_digit(item):
			digits += 1
		elif is_upper(item) or is_lower(item):
			letters += 1
			if is_vowel(item):
				vowels += 1

	print("La cadena introducida tiene", digits, "digitos y", letters, "de las cuales", vowels, "son vocales")	

```

# Ejercicio 16.

Plantee e implemente una función para cambiar a mayúsculas un carácter (entre la a y la z). Además de esta función, implemente un programa que cambie las letras de un string y las ponga todas en mayúsculas.

### Solución


```python

def is_lower(c: str) -> bool:
	tmp = ord(c)
	return tmp >= ord('a') and tmp <= ord('z')


def to_upper(c: str) -> str:
	if is_lower(c):
		diff = ord('a') - ord('A')
		c = chr( ord(c) - diff)

	return c


if __name__ == "__main__":
	c = input("Introduce un caracter: ")

	print(c, "en mayusculas es", to_upper(c))

```

# Ejercicio 17.

Plantee e implemente una función que devuelva el factorial de un número n.

### Solución


```python


def fact(n: int) -> int:
	total = 1

	for cnt in range(2, n + 1):
		total *= cnt

	return total


if __name__ == "__main__":
	n = int(input("Introduce un numero para calcular su factorial: "))

	print(n, "! = ", fact(n), sep="")

```

# Ejercicio 18.

Plantee e implemente una función que calcule el término N de la sucesión de Fibonacci.

### Solución


```python

def fib(n: int) -> int:
	if n == 0 or n == 1:
		return n

	n1 = 0
	n2 = 1
	for cnt in range(n-1):
		n1, n2 = n2, n1+n2

	return n2


if __name__ == "__main__":
	n = int(input("Introduce un numero: "))

	print("El termino", n, "de la sucesion de fibonacci es", fib(n))

 
```

# Ejercicio 19.

Plante e implemente una función que indique si un número entero es o no un número primo. 

### Solución


```python
import math


def is_prime(n: int) -> bool:

	cnt = int(math.sqrt(n))

	while cnt > 1:
		if n % cnt == 0:
			return False
		cnt -= 1

	return True


if __name__ == "__main__":
	n = int(input("Introduce un numero: "))

	if is_prime(n):
		print(n, "es primo")
	else:
		print(n, "no es primo")

```

# Ejercicio 21.

Plantee e implemente una función recursiva para calcular la N-ésima potencia de un número entero.

### Solución


```python

def power(x: int, n: int) -> int:
	if n <= 0:
		return 1
	elif n % 2 == 0:
		tmp = power(x, n/2)
		return tmp * tmp
	else:
		tmp = power(x, (n - 1)/2)
		return tmp * tmp * x

if __name__ == "__main__":
	b = int(input("Introduce un numero entero para la base: "))
	e = int(input("Introduce un numero entero y positivo para el exponente: "))

	print("El resultado es", power(b, e))

```

# Ejercicio 22.

Plantee e implemente una función recursiva para invertir un número entero.

### Solución


```python

def size(n: int) -> int:
	if n == 0:
		return 1

	cnt = 0
	while n > 0:
		n //= 10
		cnt += 1

	return cnt

def reverse(n: int) -> int:
	if n < 10:
		return n

	tmp = (n % 10) * 10 ** (size(n) - 1)
	return tmp + reverse(n // 10)	


if __name__ == "__main__":
	n = int(input("Introduce un numero entero: "))
	print(n, "<=>", reverse(n))

```

# Ejercicio 23.

Plantee e implemente una función recursiva para determinar si un número natural es capicúa. 

### Solución


```python

def size(n: int) -> int:
	if n == 0:
		return 1

	cnt = 0
	while n > 0:
		n //= 10
		cnt += 1

	return cnt

def get_highest_cypher(n: int) -> int:
	if n < 0:
		n *= -1

	last = n
	while n != 0:
		last = n
		n //= 10

	return last

def is_palindrome(n: int) -> bool:
	if n < 0:
		n *= -1

	if n == 0:
		return True

	high = get_highest_cypher(n)
	low  = n % 10
	tmp = (n - high * 10**(size(n) - 1) ) // 10
	return low == high and is_palindrome(tmp)


if __name__ == "__main__":
	n = int(input("Introduce un numero entero: "))
	print("El numero", n, "es" if is_palindrome(n) else "no es", "capicua") 


```

# Ejercicio 24.

Plantee e implemente una función recursiva que devuelva la suma de las cifras de un número entero > 0. Por ejemplo, si el número es 1256 la función devolvería 14.

### Solución


```python

def foo(n: int) -> int:
	if n < 10:
		return n
	else:
		return foo(n // 10) + n % 10

if __name__ == "__main__":
	number = int(input("Introduce un numero entero: "))
	print("La suma de las cifras del numero", number, "es", foo(number))


```

# Ejercicio 25.

Plantee e implemente una función recursiva para determinar el término N de la sucesión de Fibonacci.

### Solución


```python

def fibonacci(n: int) -> int:
	if n > 1:
		return fibonacci(n -1) + fibonacci(n -2)
	elif n == 0 or n == 1:
		return n
	else:
		return -1

if __name__ == "__main__":
	n = int(input("Introduce un numero entero positivo: "))
	print("El termino", n, " de fibonacci se corresponde con el numero", fibonacci(n))


```

