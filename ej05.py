

def floyd(line: int):
	cnt = 1
	for row in range(1, line + 1):
		for col in range(row):
			print(cnt, end="\t")
			cnt += 1
		print("")


if __name__ == "__main__":
	n = int(input("Introduce el numero de lineas a mostrar del triangulo de floyd: "))
	
	floyd(n)

