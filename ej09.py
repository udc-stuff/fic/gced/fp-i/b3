def between_range(minimum: int, maximum: int) -> int:    
    message = f"Introduce un numero entero dentro del rango [{minimum}, {maximum}]: "
    n = int(input(message))
    
    while n < minimum or n > maximum:
        n = int(input(message))
        
    return n

if __name__ == "__main__":
    
    minimum = int(input("Indica el límite inferior del intervalo: "))
    maximum = int(input("Indica el límite superior del intervalo: "))
    n = between_range(minimum, maximum)
    print("El numero introducido fue: ", n)

    n = between_range(0, 5)
    print("El numero introducido fue: ", n)
