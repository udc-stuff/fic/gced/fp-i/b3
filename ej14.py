

def is_upper(c: str) -> bool:
	tmp = ord(c)
	return tmp >= ord('A') and tmp <= ord('Z')

def is_lower(c: str) -> bool:
	tmp = ord(c)
	return tmp >= ord('a') and tmp <= ord('z')

if __name__ == "__main__":
	c = input("Introduce un caracter: ")

	if is_upper(c):
		print(c, "es una letra en mayusculas")
	else:
		print(c, "no es una letra en mayusculas")

	if is_lower(c):
		print(c, "es una letra en minusculas")
	else:
		print(c, "no es una letra en minusculas")


