
def polynomial(x: float) -> float:
	return 3 * pow(x, 5) - 2 * pow(x, 4) + pow(x, 3) - 2 * pow(x, 2) - 3 * x + 4


if __name__ == "__main__":
	n = float(input("Introduce un numero para calcular el polinomio"))

	print('El valor del polinomio es', polynomial(n))

