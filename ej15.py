

def is_vowel(c: str) -> bool:
	return c in ['A', 'a', 'E', 'e', 'I', 'i', 'O', 'o', 'U', 'u']


def is_digit(c: str) -> bool:
	tmp = ord(c)
	return tmp >= ord('0') and tmp <= ord('9')


def is_upper(c: str) -> bool:
	tmp = ord(c)
	return tmp >= ord('A') and tmp <= ord('Z')


def is_lower(c: str) -> bool:
	tmp = ord(c)
	return tmp >= ord('a') and tmp <= ord('z')


if __name__ == "__main__":
	s = input("Introduce una cadena: ")
	digits = 0
	letters = 0
	vowels = 0

	for item in s:
		if is_digit(item):
			digits += 1
		elif is_upper(item) or is_lower(item):
			letters += 1
			if is_vowel(item):
				vowels += 1

	print("La cadena introducida tiene", digits, "digitos y", letters, "de las cuales", vowels, "son vocales")	

