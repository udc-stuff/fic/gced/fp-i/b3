
def check_date(d: int, m: int, y: int) -> bool:
	if d < 1 or m < 1 or y < 1:
		return False

	if m > 12:
		return False

	if m == 2:
		leap_year = y % 4 == 0 and (y % 100 != 0 or y % 400 == 0)
		if leap_year:
			return d <= 29
		else:
			return d <= 28
	elif m == 4 or m == 6 or m == 9 or m == 11:
		return d <= 30
	else:
		return d <= 31


if __name__ == "__main__":
	d = int(input("Introduce un dia: "))
	m = int(input("Introduce un mes: "))
	y = int(input("Introduce un ano: "))

	if check_date(d, m, y):
		print(d, "/", m, "/", y, " es una fecha valida", sep="")
	else:
		print(d, "/", m, "/", y, " no es una fecha valida", sep="")
