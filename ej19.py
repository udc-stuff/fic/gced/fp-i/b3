import math


def is_prime(n: int) -> bool:

	cnt = int(math.sqrt(n))

	while cnt > 1:
		if n % cnt == 0:
			return False
		cnt -= 1

	return True


if __name__ == "__main__":
	n = int(input("Introduce un numero: "))

	if is_prime(n):
		print(n, "es primo")
	else:
		print(n, "no es primo")

