

def is_digit(c: str) -> bool:
	tmp = ord(c)
	return tmp >= ord('0') and tmp <= ord('9')

if __name__ == "__main__":
	c = input("Introduce un caracter: ")

	if is_digit(c):
		print(c, "es un digito")
	else:
		print(c, "no es un ditigo")

