

def floyd(n: int):
	cnt = 1
	row = 1
	while cnt <= n:
		col = 1
		while col <= row and cnt <= n:
			print(cnt, end="\t")
			cnt += 1
			col += 1
		print("")
		row += 1


if __name__ == "__main__":
	n = int(input("Introduce un numero para dibujar el triangulo de floyd: "))
	
	floyd(n)
