
def size(n: int) -> int:
	if n == 0:
		return 1

	cnt = 0
	while n > 0:
		n //= 10
		cnt += 1

	return cnt

def get_highest_cypher(n: int) -> int:
	if n < 0:
		n *= -1

	last = n
	while n != 0:
		last = n
		n //= 10

	return last

def is_palindrome(n: int) -> bool:
	if n < 0:
		n *= -1

	if n == 0:
		return True

	high = get_highest_cypher(n)
	low  = n % 10
	tmp = (n - high * 10**(size(n) - 1) ) // 10
	return low == high and is_palindrome(tmp)


if __name__ == "__main__":
	n = int(input("Introduce un numero entero: "))
	print("El numero", n, "es" if is_palindrome(n) else "no es", "capicua") 

