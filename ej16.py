
def is_lower(c: str) -> bool:
	tmp = ord(c)
	return tmp >= ord('a') and tmp <= ord('z')


def to_upper(c: str) -> str:
	if is_lower(c):
		diff = ord('a') - ord('A')
		c = chr( ord(c) - diff)

	return c


if __name__ == "__main__":
	c = input("Introduce un caracter: ")

	print(c, "en mayusculas es", to_upper(c))

