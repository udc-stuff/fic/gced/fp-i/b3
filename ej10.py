


def foo(n: int) -> int:
	total = 0
	for cnt in range(1, n + 1):
		total += cnt

	return total

if __name__ == "__main__":
	n = int(input("Introduce un numero entero: "))

	print("La suma es: ", foo(n))

