

def fact(n: int) -> int:
	total = 1

	for cnt in range(2, n + 1):
		total *= cnt

	return total


if __name__ == "__main__":
	n = int(input("Introduce un numero para calcular su factorial: "))

	print(n, "! = ", fact(n), sep="")

