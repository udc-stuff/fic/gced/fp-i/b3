

def power(x: int, n: int) -> int:
	if n <= 0:
		return 1
	elif n % 2 == 0:
		tmp = power(x, n/2)
		return tmp * tmp
	else:
		tmp = power(x, (n - 1)/2)
		return tmp * tmp * x

if __name__ == "__main__":
	b = int(input("Introduce un numero entero para la base: "))
	e = int(input("Introduce un numero entero y positivo para el exponente: "))

	print("El resultado es", power(b, e))

