

def foo(n: int) -> str:
	if n >= 0:
		return 'P'
	else:
		return 'N'


if __name__ == "__main__":
	n = int(input("Introduce un numero entero: "))

	print(n, "es", foo(n))

