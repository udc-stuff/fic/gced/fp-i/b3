

def size(n: int) -> int:
	if n == 0:
		return 1

	cnt = 0
	while n > 0:
		n //= 10
		cnt += 1

	return cnt

def reverse(n: int) -> int:
	if n < 10:
		return n

	tmp = (n % 10) * 10 ** (size(n) - 1)
	return tmp + reverse(n // 10)	


if __name__ == "__main__":
	n = int(input("Introduce un numero entero: "))
	print(n, "<=>", reverse(n))

