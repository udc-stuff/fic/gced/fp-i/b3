

def fibonacci(n: int) -> int:
	if n > 1:
		return fibonacci(n -1) + fibonacci(n -2)
	elif n == 0 or n == 1:
		return n
	else:
		return -1

if __name__ == "__main__":
	n = int(input("Introduce un numero entero positivo: "))
	print("El termino", n, " de fibonacci se corresponde con el numero", fibonacci(n))

