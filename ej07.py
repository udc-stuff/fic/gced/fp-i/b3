

def next_month(n: int) -> int:
	return (n % 12) + 1

if __name__ == "__main__":
	n = int(input("Introduce un numero de mes: "))

	print("El mes siguiente a", n, "es", next_month(n))
